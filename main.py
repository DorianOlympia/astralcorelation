import ToBuyOrNotToBuy
import sys
from urllib import error


def executeProgram():
    if(len(sys.argv) < 8):
        print('Give an appropriate company name')
        return
    companyName = sys.argv[1]
    bdate = sys.argv[2] + '-' + sys.argv[3] + '-' + sys.argv[4]
    edate = sys.argv[5] + '-' + sys.argv[6] + '-' + sys.argv[7]
    #create an instance of my main CorelationFinder class
    myFinder = ToBuyOrNotToBuy.CorelationFinder.CorelationFinder()
    #getting stock price of a given company 150 - is a maximum amount of days taken into account
    try:
        stockInfo = myFinder.getStockValueInformation(companyName, bdate, edate, 150)
    except error.HTTPError:
        print('Given name doesn''t exist in the database or you have given invalid time (check the order of the begin and end date).')
        return
    #result of the program
    progRes = myFinder.calculateBestCorelation(stockInfo, companyName)
    print('Program executed! For detailed result, open output text file.')
    print('For given parameters, the best correlation is:', 'Distance to earth (in a.u.)' if progRes[1] == 'getEarthDistance' else 'the phase (in %)', 'of the', progRes[2])
    print('In such case, correlation coefficient equals', str(progRes[0][0]))

if __name__ == '__main__':
    executeProgram()











