ToBuyOrNotToBuy is a program, which finds the best astral corelation for given company stock price.

#HOW TO INSTALL
This program requires:
    - gfortran (use apt-get)
    - libatlas-base-dev (use apt-get)
    - PyEphem (use pip3)
    - Scipy (use pip3)
    - Numpy (use pip3)
    - ystockquote (use pip3)

#HOW TO USE
Simply launch it using 'python3 main.py [Company Name] [YEAR] [MONTH (two digits)][DAY (two digits)] [YEAR] [MONTH (two digits)][DAY (two digits)]',
where [Company name] is an abbreviation of the full name.First date is the begin date, second one is the end date.
You can find all the abbreviations on the yahoo finance website. The program finds the best corelation and print the result
to a text file. Inside it you can find two sections separated by multiple '-' signs. Upper one applies to company stock market price,
lower one is a found astral corelation. You can easily use this text file to draw a graph (using tools like MS Excel).

#EXAMPLE
python3 main.py S 2015 05 10 2015 08 10
getPhase - the phase in %
getEarthDistance - distance to Earth in a.u.