import ephem
from scipy.stats.stats import pearsonr
import ystockquote

class CorelationFinder:
    #the criterions Im taking into account while looking for corelation.
    criterions = ['getPhase', 'getEarthDistance']

    #the main method, looking for the best corelation and returning its details
    def calculateBestCorelation(self, stockInfo, companyName):
        #take into account 8 planets and the moon
        avaliablePlanets = [z for x,y,z in ephem._libastro.builtin_planets() if x < 10 and z != 'Sun']
        #acquire required date
        dates = stockInfo[0]
        #acquire stock values in given dates
        stockVals = stockInfo[1]
        #calculate corelation coefficient for every planet and every criterium
        result = self.checkAllPossibleCorelations(stockInfo, stockVals, avaliablePlanets)
        (maxCoefficient, maxCriterion, maxCoreIdx) = result
        result = (maxCoefficient, maxCriterion, avaliablePlanets[maxCoreIdx])
        #save acquired data to the output file
        self.saveResultToFile(companyName, stockVals, self.getAstralObjectInfo(avaliablePlanets[maxCoreIdx], dates, maxCriterion), dates, result)
        return result

    #get information about stock market (company close day price)
    def getStockValueInformation(self, name, begin, end, coeficient):
        wholeHist = ystockquote.get_historical_prices(name, begin, end)
        #constrain the whole list to the list of desired maximum length, so that the data is equally spreaded over the timeline
        wholeHist = {y:wholeHist[y] for x, y in enumerate(sorted(wholeHist)) if x % self.calculateCoeficient(len(wholeHist),coeficient) == 0}
        closeStockValues = [(x.replace('-', '/'), wholeHist[x]['Adj Close']) for x in wholeHist]
        closeStockValues = sorted(closeStockValues)
        #return a tuple of company stock price during the period of time x - date, y - price on the 'x' day
        return ([x for x,y in closeStockValues],[float(y)for x,y in closeStockValues])

    #calculates the coefficient (in order to constrain the history values of company)
    def calculateCoeficient(self, dataLength, desiredLength):
        res = int(dataLength/desiredLength)
        return res + 1 if res > 1 else 1

    def getAstralObjectInfo(self, name, dates, infoType):
        """acquire desired info (infoType) on the given day (dates) using astral object called 'name'"""
        sObject = getattr(ephem, name)()
        res = [getattr(self, infoType)(date, sObject) for date in dates]
        return res

    def getEarthDistance(self, date, obj):
        obj.compute(date)
        return float(obj.earth_distance)

    def getPhase(self, date, obj):
        obj.compute(date)
        return float(obj.phase)

    #calcInfo acquired from checkAllPossibleCorelations
    def saveResultToFile(self, companyName, stock, astro, days, calcInfo):
        print('saving')
        with open('output', 'w', encoding='utf-8') as output:
            output.write('company: ' + companyName + '; astral Object: ' + calcInfo[2] + '; attribute: ' + calcInfo[1] + '; coefficient: ' + str(calcInfo[0][0]) + '\n\n')
            output.write(''.join(str(x) + ' ' + y + '\n' for x,y in zip(stock, days)))
            output.write('------------------------------\n')
            output.write(''.join(str(x) + ' ' + y + '\n' for x,y in zip(astro, days)))

    def checkAllPossibleCorelations(self, stockInfo, stockVals, avaliablePlanets):
        """returns a tuple: (current Coefficient, current Criterion", idx of the planet which fulfils criterion"""
        maxCoefficient = (0, 0)
        maxCriterion = 'N/A'
        maxCoreIdx = -1
        for criterion in CorelationFinder.criterions:
            #calculate corelation coefficients for each planet and each criterion
            coreVals = [pearsonr(self.getAstralObjectInfo(planet, stockInfo[0], criterion), stockVals) for planet in avaliablePlanets]
            currCoefficient = max(coreVals, key=lambda x: abs(x[0]))
            #find the maximum coefficient
            if(abs(maxCoefficient[0]) < abs(currCoefficient[0])):
                maxCoefficient = currCoefficient
                maxCriterion = criterion
                maxCoreIdx = coreVals.index(maxCoefficient)
        return (maxCoefficient, maxCriterion, maxCoreIdx)
